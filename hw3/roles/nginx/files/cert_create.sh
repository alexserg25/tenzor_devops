#!/bin/bash
## создание самоподписного сертификата

dir=`dirname $0`

CN="node1"
key_file=$dir/${CN}_private_key.pem
pub_cert=$dir/${CN}_public_cert.cer

if [ ! -f $key_file ]; then
  openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout $key_file -out $pub_cert -subj "/C=RU/ST=Udmurt Republic/L=Votkinsk/O=Home work/OU=home/CN=${CN}"
fi


