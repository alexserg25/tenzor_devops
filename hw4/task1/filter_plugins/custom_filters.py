#!/usr/bin/python

import re

from ansible.errors import (
    AnsibleFilterTypeError
)


def hw_addr_filter(hw_addr):
    '''
        Harware address normalization
    '''
    if not isinstance(hw_addr, str):
        raise AnsibleFilterTypeError("String type is expected, "
                                     "got type %s instead" % type(hw_addr))
    if (len(hw_addr) % 2 ):
        raise AnsibleFilterTypeError("String length is uneven")

    regexp = "^[A-F0-9]+$"
    if not (re.match(regexp, hw_addr)):
        raise AnsibleFilterTypeError("String contains invalid characters")
		
    return ":".join(re.findall(r'.{2}', hw_addr))


class FilterModule(object):
    def filters(self):
        return {
            'hw_addr_filter': hw_addr_filter
        }
