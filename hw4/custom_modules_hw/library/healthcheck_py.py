#!/usr/bin/python
# -*- coding: utf-8 -*-


from ansible.module_utils.basic import AnsibleModule

import requests;

DOCUMENTATION = r'''
---
module: healthcheck
author: Pupkin V.
short_description: healthcheck of site
description:
  - healthcheck of site with or without TLS
version_added: 1.0.0
requirements:
  - requests
  - python >= 3.6
options:
  addr:
    description:
      - Address of site we want to check
      - This is a required parameter
    type: str
  tls:
    description:
      - Whether site using certificates or not
      - Default value is 'True'
    type: bool
'''

EXAMPLES = r'''
- name: Check availability of site
  healthcheck:
    addr: mysite.example
  connection: local

- name: Check availability of site without certs
  healthcheck:
    addr: mysite.example
    tls: false
  connection: local
'''

RETURN = r'''
msg:
  description: Errors if occured
  returned: always
  type: str
  sample: ""
site_status:
  description: State status
  returned: always
  type: str
  sample: Available
rc:
  description: Return code
  returned: always
  type: int
  sample: 200
'''

def get_status_code(addr, tls):

    if (tls):
        addr = "https://" + addr
    else:
        addr = "http://" + addr

    site_status = "Available"
    msg = ""
    failed = False
    try:
        response = requests.get(addr, verify = False)
        r_code = str(response.status_code)

        if (len(r_code) == 3):
            firs_symbol = r_code[0]
            if (firs_symbol == "4"):
                msg = "Client error"
                failed = True
            elif (firs_symbol == "5"):
                msg = "Server error"
                failed = True
        else:
            failed = True
            msg = "Incorrect code received"

    except Exception as err:
        msg = "Failed to get status code. " + str(err)+ "  " + str(type(err))
        r_code = "0"
        failed = True

    if failed:
        site_status = "Unavailable"

    return(failed, msg, site_status, int(r_code))


def main():
    # Аргументы для модуля
    arguments = dict(
        addr=dict(required=True, type='str'),
        tls=dict(type='bool', default="True")
    )
    # Создаем объект - модуль
    module = AnsibleModule(
        argument_spec=arguments,
        supports_check_mode=False
    )
    # Получаем аргументы
    addr = module.params["addr"]
    tls = module.params["tls"]

    state = get_status_code(addr, tls)
    
    if state[0]:
        module.fail_json(
            changed = False,
            failed = state[0],
            msg = state[1],
            site_status = state[2],
            rc = state[3]
        )
    else:
         module.exit_json(
            changed=False,
            failed = state[0],
            msg = state[1],
            site_status = state[2],
            rc = state[3]
        )

if __name__ == "__main__":
    main()
