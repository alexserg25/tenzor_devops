#!/bin/bash
# WANT_JSON

addr=$(cat $1 | grep -Po '(?<="addr": ")(.*?)(?=")')
tls=$(cat $1 | grep -Po '(?<="tls": )(.*?)(?=,)')

if grep -qi 'false' <<< $tls; then
  link=http://$addr
else
  link=https://$addr
fi

site_status="Unavailable"
failed=true

return_code="$(curl -s -o /dev/null -w "%{http_code}" --insecure $link 2> /dev/null)"

if [ ${#return_code} -eq 3 ]; then
  case "${return_code:0:1}" in
    0  )  msg="Unknown error";;
    4  )  msg="Client error";;
    5  )  msg="Server error";;
    *  )  failed=false;
          site_status="Available";;
  esac
else
  msg="Incorrect code received"
fi

echo "{\"rc\": $return_code, \"site_status\": \"$site_status\", \"msg\": \"$msg\", \"failed\": $failed, \"changed\": false}"
